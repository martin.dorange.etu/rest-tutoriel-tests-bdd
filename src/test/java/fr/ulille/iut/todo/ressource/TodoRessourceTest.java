package fr.ulille.iut.todo.ressource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.todo.BDDFactory;
import fr.ulille.iut.todo.DebugMapper;
import fr.ulille.iut.todo.dao.TacheDAO;
import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class TodoRessourceTest extends JerseyTest {
    private TacheDAO dao;

    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        ResourceConfig rc = new ResourceConfig(TodoRessource.class);
        // Dé-commenter pour avoir la trace des requêtes et réponses
        // rc.register(new
        // LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
        // Level.INFO,
        // LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));
        // Dé-commenter pour avoir une trace des erreurs internes serveur (les tests sur
        // le code de retour vont échouer)
        // rc.register(DebugMapper.class);

        return rc;
    }

    @Before
    public void setEnvUp() {
        dao = BDDFactory.buildDao(TacheDAO.class);
        dao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
        dao.dropTable();
    }

    @Test
    public void get_should_return_empty_list_at_startup() {
        List<Tache> answer = target("taches").request(MediaType.APPLICATION_JSON).get(new GenericType<List<Tache>>() {
        });
        assertEquals(new ArrayList<Tache>(), answer);
    }

    @Test
    public void post_should_return_201_location_and_task() {
        CreationTacheDTO dto = new CreationTacheDTO();
        dto.setNom("test");
        dto.setDescription("description test");

        Response response = target("taches").request().post(Entity.json(dto));
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        Tache tache = response.readEntity(Tache.class);
        assertEquals(target("taches").getUri().toString() + "/" + tache.getId().toString(),
                response.getHeaderString("Location"));

        assertEquals(dto.getNom(), tache.getNom());
        assertEquals(dto.getDescription(), tache.getDescription());
    }

    @Test
    public void get_with_id_should_return_task() {
        Tache tache = new Tache();
        tache.setNom("test");
        tache.setDescription("description");
        dao.insert(tache);

        Tache returned = target("taches").path(tache.getId().toString()).request().get(Tache.class);

        assertEquals(tache, returned);
    }

    @Test
    public void get_with_wrong_id_should_return_404() {
    	try {
			Tache tache = new Tache();
			tache.setNom("test");
			tache.setDescription("description");

			dao.insert(tache);
			Tache returned = target("taches").path(tache.getId().toString()).request().get(Tache.class);

			assertEquals(tache, returned);
		} catch (Exception e) {
			assertEquals("HTTP 404 Not Found",e.getMessage());
		}
    }

    @Test
    public void get_for_description_should_work_with_existing_task() {
    	Tache tache=new Tache();
    	tache.setNom("test");
    	tache.setDescription("description");
    	
    	dao.insert(tache);
    	Tache returned = target("taches").path(tache.getId().toString()).request().get(Tache.class);
    	
    	assertEquals("description",returned.getDescription());
    	
    }

    @Test
    public void get_for_description_with_wrong_id_should_return_404() {
    	try {
    		Tache tache=new Tache();
        	tache.setNom("test");
        	tache.setDescription("description");
        	
        	dao.insert(tache);
        	Tache returned = target("taches").path(tache.getId().toString()).request().get(Tache.class);
        	
        	assertEquals("description",returned.getDescription());
    	}catch(Exception e) {
    		assertEquals("HTTP 404 Not Found",e.getMessage());
    	}
    }

    @Test
    public void delete_should_remove_task_and_return_204() {
    }

    @Test
    public void delete_with_wrong_id_should_return_404() {
    }

    @Test
    public void put_should_replace_existing_task_values_return_200_and_task() {
    }

    @Test
    public void partial_put_should_fail_with_400() {
    } 

    @Test
    public void post_with_form_data_should_return_201_location_and_task() {
    	Form form=new Form();
        form.param("nom","test");
        form.param("description","description test");

        Response response = target("taches").request(MediaType.APPLICATION_FORM_URLENCODED).post(Entity.form(form));
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        Tache tache = response.readEntity(Tache.class);
        assertEquals(target("taches").getUri().toString() + "/" + tache.getId().toString(),
                response.getHeaderString("Location"));

        Map<String, List<String>> returned=form.asMap();
        System.out.println(returned.toString());
//        assertEquals(dto.getNom(), tache.getNom());
//        assertEquals(dto.getDescription(), tache.getDescription());
    	
    }
}
