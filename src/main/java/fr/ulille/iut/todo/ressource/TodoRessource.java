package fr.ulille.iut.todo.ressource;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import fr.ulille.iut.todo.dto.CreationTacheDTO;
import fr.ulille.iut.todo.service.Tache;
import fr.ulille.iut.todo.service.TodoService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.EntityTag;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

@Path("taches")
@Produces(MediaType.APPLICATION_JSON)
public class TodoRessource {
    private final static Logger LOGGER = Logger.getLogger(TodoRessource.class.getName());

    private TodoService todoService = new TodoService();

    @Context
    private UriInfo uri;
    @Context
    Request request;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Tache> getAll() {
        LOGGER.info("getAll()");

        return todoService.getAll();
    }
    
    @GET
    @Path("{id}")
    public Tache getTache(@PathParam("id") UUID id) {
    	LOGGER.info("getTache()");
    	
    	return todoService.getTache(id);
    }
    
    @GET
    @Path("{id}/description")
    public String getDescription(@PathParam("id") UUID id) {
    	LOGGER.info("getDescription()");
    	
    	return todoService.getTache(id).getDescription();
    }

    @POST
    public Response createTache(CreationTacheDTO tacheDto) {
        LOGGER.info("createTache()");

        Tache tache = Tache.fromCreationTacheDTO(tacheDto);
        todoService.addTache(tache);
        URI location = uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

        EntityTag etag = new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }
    
    @POST
    @Consumes (MediaType.APPLICATION_FORM_URLENCODED)
    public Response createTacheFromForm(MultivaluedMap<String, String> formParams) {
    	LOGGER.info("createTacheFromForm()");
    	
    	Tache tache = new Tache();
    	String name= formParams.get("nom").get(0);
    	System.out.println(name);
    	tache.setNom(name);
    	todoService.addTache(tache);

    	URI location= uri.getAbsolutePathBuilder().path(tache.getId().toString()).build();

    	EntityTag etag =new EntityTag(Integer.toString(tache.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        if (builder == null) {
            builder = Response.created(location);
            builder.tag(etag);
            builder.entity(tache);
        }
        return builder.build();
    }

}
