package fr.ulille.iut.todo.service;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.todo.BDDFactory;
import fr.ulille.iut.todo.dao.TacheDAO;

public class TodoService {
    private TacheDAO taches;

    public TodoService() {
        taches = BDDFactory.buildDao(TacheDAO.class);
        taches.createTable();
    }

    public Tache getTache(UUID id) {  	
        return taches.getById(id.toString());
    }

    public List<Tache> getAll() {
        return taches.getAll();
    }

    public void addTache(Tache newTache) {
        taches.insert(newTache);
    }

    public int deleteTache(String id) {
        return 0 ;
    }

    public void updateTache(Tache tache) {
        return;
    }
}
